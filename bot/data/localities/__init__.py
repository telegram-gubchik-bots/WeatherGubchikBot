from .ua_ukr_localities import UA_UKR_LOCALITIES
from .ua_abroad_localities import UA_ABROAD_LOCALITIES

from .en_urk_localities import EN_UKR_LOCALITIES
from .en_abroad_localities import EN_ABROAD_LOCALITIES

from .ru_ukr_localities import RU_UKR_LOCALITIES
from .ru_abroad_localities import RU_ABROAD_LOCALITIES
